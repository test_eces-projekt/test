public class Main {

	public static void main(String args[]) {
		// Variable zum Überpruefen, ob alle Tests durchlaufen
		boolean passed = true;

		// erstellen von Testvariablen
		int[] testvector1 = { 1, 2, 3 };
		int[] testvector2 = { 3, 2, 1 };
		int[] testvector3 = { 1, 1, 1 };
		int[] testvector4 = { 3, 1, 3 };
		int[] testvector5 = { 1, 0, 0 };
		int[] testvector6 = { 0, 1, 0 };

		// Testen des Skalarproduktes
		int skpr1 = skalarProdukt(testvector1, testvector2);
		int skpr2 = skalarProdukt(testvector4, testvector3);
		if (skpr1 == 10 && skpr2 == 7) {
			System.out.println("Methode fuer das Skalarprodukt liefert richtiges Ergebnis");
		} else {
			System.out.println("Methode fuer das Skalarprodukt liefert falsches Ergebnis");
			passed = false;
		}

		// Testen des Kreuzproduktes
		int[] krpr1 = kreuzProdukt(testvector5, testvector6);
		int[] krpr2 = kreuzProdukt(testvector1, testvector2);
		if (krpr1[0] == 0 && krpr1[1] == 0 && krpr1[2] == 1 && krpr2[0] == -4 && krpr2[1] == 8 && krpr2[2] == -4) {
			System.out.println("Methode fuer Kreuzprodukt liefert richtiges Ergebnis");
		} else {
			System.out.println("Methode fuer das Kreuzprodukt liefert ein falsches Ergebnis");
			passed = false;
		}

		// finales Ergebniss, ob alle Tests durchlaufen
		if (passed)
			System.out.println("\n" + "Alle Tests bestanden");
	}

	/**
	 * Die Methode soll das Skalarprodukt von zwei Vektoren berechnen und dieses
	 * dann zurueckgeben.
	 *
	 * @param a Vektor zur Berechnung des Kreuzproduktes als Array der Laenge 3
	 * @param b Vektor zur Berechnung des Kreuzproduktes als Array der Laenge 3
	 * @return gibt das Skalarprodukt der beiden Uebergebenen Vektoren ans int
	 *         zurueck
	 */

	public static Integer skalarProdukt(int[] a, int[] b) {
		int ergebnis = 0;
		if (a.length != b.length) {
			return null;
		} else {
			for (int i = 0; i < a.length; i++) {
				ergebnis += a[i] * b[i];
			}
			return ergebnis;
		}
	}

	/**
	 * Die Methode soll das Kreuzprodukt der beiden uebergebenen Vektoren berechnen
	 * und das Ergebnis dann zurueckgeben.
	 *
	 * @param a Vektor zur Berechnung des Kreuzproduktes als Array der Laenge 3
	 * @param b Vektor zur Berechnung des Kreuzproduktes als Array der Laenge 3
	 * @return gibt das Kreuzprodukt der beiden Uebergebenen Vektoren zurueck als
	 *         Array der Laenge 3
	 */
	public static int[] kreuzProdukt(int[] a, int[] b) {
		if (a.length != b.length && a.length != 3 && b.length != 3) {
			return null;
		} else {

			Func cr = (i, j) -> a[i] * b[j] - a[j] * b[i];
			return new int[] { cr.apply(1, 2), cr.apply(2, 0), cr.apply(0, 1) };
		}
	}

	@FunctionalInterface
	public interface Func {
		public int apply(int i, int j);
	}

}
